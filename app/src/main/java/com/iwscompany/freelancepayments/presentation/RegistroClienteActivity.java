package com.iwscompany.freelancepayments.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iwscompany.freelancepayments.R;
import com.iwscompany.freelancepayments.bo.ClienteBo;
import com.iwscompany.freelancepayments.dao.ClienteDao;
import com.iwscompany.freelancepayments.dto.ClienteDto;
import com.iwscompany.freelancepayments.utils.ExperienciaUsuario;

import java.util.HashMap;
import java.util.Map;

public class RegistroClienteActivity extends AppCompatActivity {
    ClienteBo clienteBo= new ClienteBo();
    Button btnGuardar;
    EditText etNombre,etResponsable,etDireccion,etEmail,etTelefono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Window window = this.getWindow();
        ExperienciaUsuario.cambiarColorStatusBar(window,this);

        btnGuardar=findViewById(R.id.btnGuardar);
        etNombre=findViewById(R.id.etNombre);
        etResponsable=findViewById(R.id.etResponsable);
        etDireccion=findViewById(R.id.etDireccion);
        etEmail=findViewById(R.id.etEmail);
        etTelefono=findViewById(R.id.etTelefono);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,Object>clientes=new HashMap<>();
                clientes.put("nombre",etNombre.getText().toString());
                clientes.put("responsable",etResponsable.getText().toString());
                clientes.put("direccion",etDireccion.getText().toString());
                clientes.put("email",etEmail.getText().toString());
                clientes.put("telefono",etTelefono.getText().toString());

                clienteBo.guardarCliente(clientes);
            }
        });

    }
}
