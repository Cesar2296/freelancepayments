package com.iwscompany.freelancepayments.presentation;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iwscompany.freelancepayments.R;
import com.iwscompany.freelancepayments.dto.ClienteDto;
import com.iwscompany.freelancepayments.utils.ExperienciaUsuario;

import java.util.ArrayList;

public class ClientesActivity extends AppCompatActivity {
    ListView listClientes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Window window = this.getWindow();
        ExperienciaUsuario.cambiarColorStatusBar(window,this);

        listClientes=findViewById(R.id.list_clientes);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("clientes");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayAdapter<String> adaptador;
                ArrayList<String> listado= new ArrayList<String>();
                for (DataSnapshot data:dataSnapshot.getChildren()) {
                    ClienteDto cliente=data.getValue(ClienteDto.class);
                    listado.add(cliente.getNombre());
                }
                adaptador=new ArrayAdapter<String>(ClientesActivity.this,android.R.layout.simple_list_item_1,listado);
                listClientes.setAdapter(adaptador);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("LEEDB",databaseError.getMessage());
            }
        });

    }

}
