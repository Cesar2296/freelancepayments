package com.iwscompany.freelancepayments.dao;

import java.util.Map;

public class ClienteDao extends FirebaseDB{
    private final String coleccion="clientes";
    public void guardarCliente(Map<String,Object>datos){
        save(coleccion,datos);
    }
}
